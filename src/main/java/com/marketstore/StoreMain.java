package com.marketstore;

import com.marketstore.entities.BronzeCard;
import com.marketstore.entities.DiscountCard;
import com.marketstore.entities.GoldCard;
import com.marketstore.entities.SilverCard;
import com.marketstore.paydesk.PayDesk;

public class StoreMain {
    public static void main(String[] args) {
        DiscountCard gold = new GoldCard(1500);
        DiscountCard silver = new SilverCard(600);
        DiscountCard bronze = new BronzeCard(0);
        System.out.println("1. Bronze card mock data: turnover $0, purchase value $150;");
        System.out.println("");
        PayDesk.payDeskOutput(bronze, 150);
        System.out.println("");
        System.out.println("2. Silver card mock data: turnover $600, purchase value $850;");
        System.out.println("");
        PayDesk.payDeskOutput(silver, 850);
        System.out.println("");
        System.out.println("3. Gold card mock data: turnover $1500, purchase value $1300;");
        System.out.println("");
        PayDesk.payDeskOutput(gold, 1300);
    }
}
