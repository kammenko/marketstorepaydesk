package com.marketstore.controllers;

import com.marketstore.dto.InputDTO;
import com.marketstore.dto.OutputDTO;
import com.marketstore.mappers.InputToOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class StoreControl {

    @Autowired
    InputToOutput iO;


    @GetMapping(value = "initialize")
    public ModelAndView initializeStore() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("IDTO", new InputDTO());
        mav.addObject("ODTO", new OutputDTO());
        mav.setViewName("Paydesk");
        return mav;
    }

    @PostMapping(value = "checkout")
    public ModelAndView checkout(InputDTO iDTO, OutputDTO oDTO, ModelAndView mav) {
        oDTO = iO.InToOut(iDTO);
        mav.addObject("IDTO", iDTO);
        mav.addObject("ODTO", oDTO);
        mav.setViewName("Paydesk");
        return mav;
    }


}
