package com.marketstore.util;

import com.marketstore.entities.BronzeCard;
import com.marketstore.entities.DiscountCard;
import com.marketstore.entities.GoldCard;
import com.marketstore.entities.SilverCard;
import org.springframework.stereotype.Service;

@Service
public class Util {

    public static DiscountCard cardMaker(int id, double turnover){
        switch (id) {
            case 1: return new BronzeCard(turnover);
            case 2: return new SilverCard(turnover);
            case 3: return new GoldCard(turnover);
            default: return new DiscountCard(turnover);
        }

    }
}
