package com.marketstore.paydesk;

import com.marketstore.entities.DiscountCard;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class PayDesk {

    public PayDesk() {
    }

    public static void payDeskOutput(DiscountCard card, double purchaseValue) {
        double discount = card.discountRate() * purchaseValue;
        double total = purchaseValue - discount;
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("Purchase value: $" + df.format(purchaseValue));
        System.out.println("");
        System.out.println("Discount rate: " + new DecimalFormat("0.0").format(card.discountRate() * 100) + "%");
        System.out.println("");
        System.out.println("Discount: $" + df.format(discount));
        System.out.println("");
        System.out.println("Total: $" + df.format(total));
    }

    public String discount(DiscountCard card, double purchaseValue) {
        return new DecimalFormat("0.00").format(card.discountRate() * purchaseValue);
    }

    public String total(DiscountCard card, double purchaseValue) {
        return new DecimalFormat("0.00").format(purchaseValue - Double.parseDouble(discount(card, purchaseValue)));
    }

    public String discountRate(DiscountCard card){
        if(card.discountRate() > 0)
        return new DecimalFormat("0.0").format(card.discountRate()*100);
        else return "0.0";
    }


}
