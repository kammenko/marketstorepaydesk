package com.marketstore.entities;

import java.util.Objects;
public class DiscountCard {

    //For owner we could make a class Person.
    private String ownerName="Luke";
    private String ownerLastName="SkyWalker";
    private double lastMonthTurnover;
    private double initialDiscountRate = 0;



    public DiscountCard() {

    }
    public DiscountCard(double lastMonthTurnover) {

        this.lastMonthTurnover = lastMonthTurnover;
    }

    public DiscountCard(String ownerName, String ownerLastName, double lastMonthTurnover) {

        this.ownerName = ownerName;
        this.ownerLastName = ownerLastName;
        this.lastMonthTurnover = lastMonthTurnover;
    }

    public String getOwnerName() {

        return ownerName;
    }

    public void setOwnerName(String ownerName) {

        this.ownerName = ownerName;
    }

    public String getOwnerLastName() {

        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {

        this.ownerLastName = ownerLastName;
    }

    public double getInitialDiscountRate() {
        return initialDiscountRate;
    }

    public void setInitialDiscountRate(double initialDiscountRate) {
        this.initialDiscountRate = initialDiscountRate;
    }

    public double getLastMonthTurnover() {
        return lastMonthTurnover;
    }

    public void setLastMonthTurnover(double lastMonthTurnover) {
        this.lastMonthTurnover = lastMonthTurnover;
    }

    public double currentPurchaseDiscount(double valueOfPurchase){
           return this.discountRate() * valueOfPurchase;
    }

    public  double discountRate(){
        return  this.initialDiscountRate ;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscountCard that = (DiscountCard) o;
        return Double.compare(that.lastMonthTurnover, lastMonthTurnover) == 0 &&
                Double.compare(that.initialDiscountRate, initialDiscountRate) == 0 &&
                Objects.equals(ownerName, that.ownerName) &&
                Objects.equals(ownerLastName, that.ownerLastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ownerName, ownerLastName, lastMonthTurnover, initialDiscountRate);
    }

    @Override
    public String toString() {
        return "DiscountCard{" +
                "ownerName='" + ownerName + '\'' +
                ", ownerLastName='" + ownerLastName + '\'' +
                ", lastMonthTurnover=" + lastMonthTurnover +
                ", initialDiscountRate=" + initialDiscountRate +
                '}';
    }
}
