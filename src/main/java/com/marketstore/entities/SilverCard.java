package com.marketstore.entities;

public class SilverCard extends DiscountCard {

    public SilverCard() {
        this.setInitialDiscountRate(0.02);
    }

    public SilverCard(double lastMonthTurnover) {
        super(lastMonthTurnover);
        this.setInitialDiscountRate(0.02);
    }

    public SilverCard(String ownerName, String ownerLastName, double lastMonthTurnover) {
        super(ownerName, ownerLastName, lastMonthTurnover);
        this.setInitialDiscountRate(0.02);
    }

    @Override
    public double discountRate() {
        double discountRate = this.getInitialDiscountRate();
        if (this.getLastMonthTurnover() > 300)
            discountRate = 0.035;
        return discountRate;
    }
}
