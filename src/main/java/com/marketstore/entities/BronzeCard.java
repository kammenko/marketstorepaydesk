package com.marketstore.entities;

public class BronzeCard extends DiscountCard {

    public BronzeCard() {
    }

    public BronzeCard(double lastMonthTurnover) {

        super(lastMonthTurnover);
    }

    public BronzeCard(String ownerName, String ownerLastName, double lastMonthTurnover) {

        super(ownerName, ownerLastName, lastMonthTurnover);
    }

    @Override
    public double discountRate() {

        if (this.getLastMonthTurnover() < 100)
            return this.getInitialDiscountRate();

        if (this.getLastMonthTurnover() >= 100 && this.getLastMonthTurnover() <= 300)
            return 0.01;
        return 0.025;
    }


}
