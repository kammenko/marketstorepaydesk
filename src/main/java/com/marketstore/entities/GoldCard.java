package com.marketstore.entities;

public class GoldCard extends DiscountCard {


    public GoldCard() {
        this.setInitialDiscountRate(0.02);
    }

    public GoldCard(double lastMonthTurnover) {
        super(lastMonthTurnover);
        this.setInitialDiscountRate(0.02);
    }

    public GoldCard(String ownerName, String ownerLastName, double lastMonthTurnover) {
        super(ownerName, ownerLastName, lastMonthTurnover);
        this.setInitialDiscountRate(0.02);
    }

    @Override
    public double discountRate() {
        double discountRate = this.getInitialDiscountRate() + (int) (this.getLastMonthTurnover()) / 100 * 0.01;
        if (discountRate < 0.1)
            return discountRate;
        else return 0.1;
    }


}
