package com.marketstore.mappers;

import com.marketstore.dto.InputDTO;
import com.marketstore.dto.OutputDTO;
import com.marketstore.entities.DiscountCard;
import com.marketstore.paydesk.PayDesk;
import com.marketstore.util.Util;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class InputToOutput {

    public OutputDTO InToOut(InputDTO iDTO) {
        OutputDTO oDTO = new OutputDTO();
        PayDesk payDesk = new PayDesk();
        DiscountCard dCard = Util.cardMaker(iDTO.getCardId(), iDTO.getTurnover());
        oDTO.setPurchaseValue(new DecimalFormat("0.00").format(iDTO.getPurchaseValue()));
        oDTO.setDiscountRate(payDesk.discountRate(dCard));
        oDTO.setDiscount(payDesk.discount(dCard, iDTO.getPurchaseValue()));
        oDTO.setTotal(payDesk.total(dCard, iDTO.getPurchaseValue()));
        return oDTO;
    }

}
