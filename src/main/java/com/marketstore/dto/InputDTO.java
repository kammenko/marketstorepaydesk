package com.marketstore.dto;

public class InputDTO {

    int cardId;
    double purchaseValue;
    double turnover;

    public InputDTO() {
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public double getPurchaseValue() {
        return purchaseValue;
    }

    public void setPurchaseValue(double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {

        this.turnover = turnover;
    }

}
