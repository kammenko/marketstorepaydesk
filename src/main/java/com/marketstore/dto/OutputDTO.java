package com.marketstore.dto;

public class OutputDTO {

    String purchaseValue;
    String discountRate;
    String discount;
    String total;

    public OutputDTO() {
    }


    public String getPurchaseValue() {
        return purchaseValue;
    }

    public void setPurchaseValue(String purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate + "%";
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = "$" + discount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = "$" + total;
    }
}
