package com.marketstore;

import com.marketstore.entities.DiscountCard;
import com.marketstore.entities.SilverCard;
import com.marketstore.paydesk.PayDesk;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaydeskApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaydeskApplication.class, args);
    }

}
