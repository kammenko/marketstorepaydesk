Download the file(zip, tar...) from the GitLab repository.
Unpack and open the folder as project with the IDE.
Let the application build with maven.
After that you can start the StoreMain class and you will get the sample data output in the console.
The other way to start the application with the user Input is through PaydeskApplication class.
By starting this class and going to "localhost:8080/initialize" in your browser you will get an interface for inputting desired data.


How the application works:
By starting the StoreMain class the 3 Discount Cards get instantiated with the last month turnover.
That automatically calculates the discount rate for each card.
After that the static method payDeskOutput from the PayDesk class is called for each of the cards with the desired purchase value.
The payDeskOutput method calculates all the values, formats them and outputs them in the console.


By starting the PaydeskApplication and opening "localhost:8080/initialize" in your browser you will be presented with the interface of picking the desired card.
After picking the card and inputting the last month turnover in the input field, you should input the purchase value.
After that click the button and the desired output will be populated.
The click on the button calls the controller method and it transfers the input to it and to the payDesk methods working in the background.
The methods calculate everything and send it to the DTO which carries the data to the front end where it populates the output fields.

Methods used in PayDesk :

payDeskOutput -  calculates everything needed and outputs the values in the console
discount - calculates the discount for the input purchase value
total - calculates the total that customer needs to pay
discountRate -  outputs cards discount rate in percentages as in the car they are stored as 0.X

All return String for better formatting.